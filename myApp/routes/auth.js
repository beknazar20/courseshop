const {Router} = require('express')
const router = Router()
const bcrypt = require('bcryptjs')
const User = require('../models/user')
router.get('/login',async (req,res)=>{
    res.render('auth/login',{
        tite:'Авторизация',
        isLogin:true,
        loginError:req.flash('loginError'),
        registerError: req.flash('registerError')
    })
})
router.post('/login',async (req,res)=>{
    try {
        const {email,password} = req.body;
        const candidate = await User.findOne({email})
        if(candidate){
            // const areSame = password === candidate.password;
            const areSame = await bcrypt.compare(password,candidate.password)
            if(areSame){
                req.session.user = candidate
                req.session.isAuthenticated = true;
                req.session.save(err=>{
                    if(err){
                        throw err
                    }else{
                     res.redirect('/')   
                    }
                })
            }else{
                 req.flash('loginError','Пароль не верный')
                res.redirect('/auth/login#login')
            }
        }else{
            req.flash('loginError','Такого пользовотеля нет')
             res.redirect('/auth/login#login')
        }
    } catch (error) {
        console.log(error)
    }
    //    const user = await User.findById('5efa28bd7f0ce20e40896483')
    // req.session.user = user
    // req.session.isAuthenticated = true;
    // req.session.save(err=>{
    //     if(err){
    //         throw err
    //     }else{
    //      res.redirect('/')   
    //     }
    // })
})
router.get('/logout',async (req,res)=>{
    // req.session.isAuthenticated = false;
    req.session.destroy(()=>{
        res.redirect('/auth/login#login')
    })
})
router.post('/register',async(req,res)=>{
    try {
        const { email, password, repeat,name} = req.body;
        const candidate = await User.findOne({email})
        if(password === repeat){
        if(candidate){
            req.flash('registerError','Пользователь уже занят')
            res.redirect('/auth/login#register')
        }else{
            const hashPassword = await bcrypt.hash(password, 10)
            const user  = new User({
                 email,password: hashPassword,name,cart:{items:[]}
            })
            await user.save()
            res.redirect('/auth/login#login')

        }
    }else{
        req.flash('registerError','не совподает');
        res.redirect('/auth/login#register')
    }
    } catch (error) {
        console.log(error)
    }
})
module.exports = router
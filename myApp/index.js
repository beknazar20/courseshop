const express = require('express')
const path = require('path')
const session = require('express-session')
const MongoStore = require('connect-mongodb-session')(session)
const flash = require('connect-flash')
const mongoose = require('mongoose')
const exphbs = require('express-handlebars')
const homeRoutes = require('./routes/home')
const cardRoutes = require('./routes/card')
const addRoutes = require('./routes/add')
const ordersRoutes = require('./routes/orders')
const coursesRoutes = require('./routes/courses')
const authRoutes = require('./routes/auth')
const varMiddleware = require('./middleware/variables')
const userMiddleware = require('./middleware/user')
const keys = require('./keys')
const app = express()
const store = new MongoStore({
  collection:'sessions',
  uri:keys.remoteURL,

})
const hbs = exphbs.create({
  defaultLayout: 'main',
  extname: 'hbs'
})

app.engine('hbs', hbs.engine)
app.set('view engine', 'hbs')
app.set('views', 'views')
// app.use( async(req,res,next)=>{
// try {
//   const user = await User.findById('5efa28bd7f0ce20e40896483')
//   req.user = user
//   next()
// } catch (error) {
//   console.log(error)
// }
// })
app.use(express.static(path.join(__dirname, 'public')))
app.use(express.urlencoded({extended: true}))
app.use(session({
  secret:keys.SESSION_SECRET,
  resave:false,
  saveUninitialized:false,
  store:store
}));
app.use(flash())
app.use(varMiddleware)
app.use(userMiddleware)

app.use('/', homeRoutes)
app.use('/add', addRoutes)
app.use('/courses', coursesRoutes)
app.use('/card', cardRoutes)
app.use('/orders', ordersRoutes)
app.use('/auth', authRoutes)

const PORT = process.env.PORT || 3001

async function start() {
  try {
    await mongoose.connect(keys.remoteURL, {
      useNewUrlParser: true
    })
    // const candidate = await User.findOne();
    // if(!candidate){
    //   const user = new User({
    //     email:'beku.00000@gmaol.com',
    //     name:'Beknazar',
    //     cart:{items:[]}
    //   })
    //  await user.save()
    // }
    app.listen(PORT, () => {
      console.log(`Server is running on port ${PORT}`)
    })
  } catch (e) {
    console.log(e)
  }
}

start()


